package com.springboot.neo4j.demo.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.springboot.neo4j.demo.model.Asset;
import com.springboot.neo4j.demo.model.Organisation;
import com.springboot.neo4j.demo.service.OrganizationService;

@RestController
@RequestMapping("/api")
public class HomeController {
	
	@Autowired
	private OrganizationService organizationService;

	@RequestMapping(value="/create-company", method=RequestMethod.POST)
	public ResponseEntity<Organisation> createCompany(@RequestBody Organisation organisation) {
		organizationService.saveOrganisation(organisation);
		return new ResponseEntity<Organisation>(organisation,HttpStatus.OK);
	}
	
	@RequestMapping(value="/organizations", method=RequestMethod.GET)
	public ResponseEntity<List<Organisation>> getOrganisations(String name) {
		List<Organisation> organisations = (List<Organisation>) organizationService.findAllOrganization();
		return new ResponseEntity<List<Organisation>>(organisations,HttpStatus.OK);
	}
	
	@RequestMapping(value="/get-organization/{companyName}", method=RequestMethod.GET)
	public Organisation getOrganisation(@PathVariable String companyName) {
		Organisation organization = organizationService.getOrganisation(companyName);
		return organization;
	}
	
	@RequestMapping(value="/add-child-to-parent/{parentId}", method=RequestMethod.PUT)
	public ResponseEntity<String> addChildToParrents(@PathVariable String parentId,@RequestBody Organisation child) {
		List<Organisation> childList = new ArrayList<>();
		childList.add(child);
		Organisation parent = organizationService.findParentOrganisation(parentId);
		parent.setChild(childList);
		organizationService.saveOrganisation(parent);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/asset-conncet/{code}", method=RequestMethod.PUT)
	public ResponseEntity<String> createAsset(@RequestBody Asset asset, @PathVariable String code) {
		List<Asset> assetList = new ArrayList<>();
		assetList.add(asset);
		Organisation organisation = organizationService.findParentOrganisation(code);
		organisation.setAsset(assetList);
		organizationService.saveOrganisation(organisation);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
}
