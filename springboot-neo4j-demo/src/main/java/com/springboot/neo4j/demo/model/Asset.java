package com.springboot.neo4j.demo.model;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Properties;
import org.neo4j.ogm.annotation.Property;

@NodeEntity
public class Asset {

	@Id
	@GeneratedValue
	@Property(name="id")
	private Long id;
	@Property(name="name")
	private String name;
	
	public Asset() {
	
	}
	
	public Asset(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
