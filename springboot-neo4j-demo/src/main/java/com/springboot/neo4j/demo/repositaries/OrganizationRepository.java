package com.springboot.neo4j.demo.repositaries;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;
import com.springboot.neo4j.demo.model.Organisation;

@Repository
public interface OrganizationRepository extends Neo4jRepository<Organisation, Long>{

	Organisation findByName(String name);

	Organisation findByCode(String string);


}
