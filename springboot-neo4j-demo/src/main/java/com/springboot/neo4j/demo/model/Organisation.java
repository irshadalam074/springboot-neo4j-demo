package com.springboot.neo4j.demo.model;

import java.util.List;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class Organisation {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String type;
	private String code;
	
	@Relationship(type="CHILD_OF",direction=Relationship.OUTGOING)
	private List<Organisation> child;
	public Organisation() {
	}
	
	@Relationship(type="CONNECT_TO", direction=Relationship.OUTGOING)
	private List<Asset> asset;
	
	public Organisation(String name, String type, String code) {
		this.name = name;
		this.type = type;
		this.code = code;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public List<Organisation> getChild() {
		return child;
	}
	public void setChild(List<Organisation> child) {
		this.child = child;
	}
	public List<Asset> getAsset() {
		return asset;
	}
	public void setAsset(List<Asset> asset) {
		this.asset = asset;
	}
	
	
}
