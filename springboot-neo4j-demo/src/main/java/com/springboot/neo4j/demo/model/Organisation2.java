package com.springboot.neo4j.demo.model;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import io.netty.handler.codec.http2.Http2FrameLogger.Direction;

@NodeEntity
public class Organisation2 {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String type;
	//private String code;
	//@Relationship(type="CHILD", direction=Relationship.INCOMING)
	//private Set<Organisation2> child = new HashSet<>();
	/*private Set<ChildOrganisation> child = new HashSet<>();*/
	
	public Organisation2(String name, String type/*, String code*/) {
		this.name = name;
		this.type = type;
		//this.code = code;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	/*public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}*/
	/*public Set<Organisation2> getChild() {
		return child;
	}
	public void setChild(Set<Organisation2> child) {
		this.child = child;
	}*/
	/*public Set<ChildOrganisation> getChild() {
		return child;
	}
	public void setChild(Set<ChildOrganisation> child) {
		this.child = child;
	}*/
	
	
}
