package com.springboot.neo4j.demo.service;

import java.util.List;

import com.springboot.neo4j.demo.model.Organisation;

public interface OrganizationService {

	void saveOrganisation(Organisation organisation);

	Organisation getOrganisation(String companyName);

	List<Organisation> findAllOrganization();

	Organisation findParentOrganisation(String parentId);

	

	
	
}
