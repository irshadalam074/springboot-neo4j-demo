package com.springboot.neo4j.demo.repositaries;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import com.springboot.neo4j.demo.model.Asset;

public interface AssetRepository extends Neo4jRepository<Asset, Long> {

	@Query("MATCH (a:Asset), (b:Organisation) WHERE a.name={0} AND b.code={1} CREATE (b)-[r: CONNECT_TO]->(a)")
	void connectAssetToOrganization(String name, String code);

	Asset findByName(String assetName);

}
