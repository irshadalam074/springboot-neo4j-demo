package com.springboot.neo4j.demo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.springboot.neo4j.demo.model.Organisation;
import com.springboot.neo4j.demo.repositaries.OrganizationRepository;
import com.springboot.neo4j.demo.service.OrganizationService;

@Service
public class OrganizationServiceImpl implements OrganizationService {

	@Autowired
	private OrganizationRepository organizationRepository;

	@Override
	public void saveOrganisation(Organisation organisation) {
		
		organizationRepository.save(organisation);
	}

	@Override
	public Organisation getOrganisation(String companyName) {
		
		return organizationRepository.findByName(companyName);
	}

	@Override
	public List<Organisation> findAllOrganization() {
		
		return (List<Organisation>) organizationRepository.findAll();
	}

	@Override
	public Organisation findParentOrganisation(String parentId) {
		
		return organizationRepository.findByCode(parentId);
	}

	
	

	
	
}
