package com.springboot.neo4j.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootNeo4jDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootNeo4jDemoApplication.class, args);
	}

}
