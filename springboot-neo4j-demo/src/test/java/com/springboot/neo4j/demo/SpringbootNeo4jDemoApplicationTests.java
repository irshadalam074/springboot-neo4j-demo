package com.springboot.neo4j.demo;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.neo4j.demo.model.Asset;
import com.springboot.neo4j.demo.model.Organisation;
import com.springboot.neo4j.demo.repositaries.AssetRepository;
import com.springboot.neo4j.demo.repositaries.OrganizationRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class SpringbootNeo4jDemoApplicationTests {

	@Autowired
	private OrganizationRepository organizationRepository;
	@Autowired
	private AssetRepository assetRepository;
	private Organisation company; 
	
	@Before
	public void setUp() {
		
		/*Create Company*/
		company = new Organisation("C1", "Company", "cmpn101");
		organizationRepository.save(company);
	}
	
	/*@Before
	public void assetSetUp() {
		
		Create Company
		company = new Organisation("C1a", "Company", "c1a101");
		organizationRepository.save(company);
	}*/
	
	@Test
	public void addChildNodeTest() {
		
		/*Add Zone*/
		//List<Organisation> zone = new ArrayList<Organisation>();
		List<Organisation> zones = new ArrayList<>();
		zones.add(new Organisation("Z1", "Zone", "zna101"));
		zones.add(new Organisation("Z2", "Zone", "zna102"));
		
		Organisation comp = organizationRepository.findByCode(company.getCode());
		comp.setChild(zones);
		organizationRepository.save(comp);
		
		/*Add Division*/
		List<Organisation> division = new ArrayList<Organisation>();
		division.add(new Organisation("D1", "Division", "dv101"));
		division.add(new Organisation("D2", "Division", "dv102"));
		
		Organisation zonedb = organizationRepository.findByCode("zna101");
		zonedb.setChild(division);
		organizationRepository.save(zonedb);
		
		/*Check z2 present*/
		Organisation companyfromdb = organizationRepository.findByCode("cmpn101");
		
		for(Organisation zone : companyfromdb.getChild()) {
			if(zone.getName()=="Z2") {
				assertEquals("Z2", zone.getName());
				System.out.println("Z2 is present inside company c1");
			}
		}
		
		/*Check D1 present*/
		Organisation zonefromdb = organizationRepository.findByCode("zna101");
		
		for(Organisation div : zonefromdb.getChild()) {
			if(div.getName()=="D1") {
				assertEquals("D1", div.getName());
				System.out.println("D1 is present");
			}
		}
	}
	
	@Test
	public void addAssetTest() {
		/*Add Zone1*/
		List<Organisation> zones = new ArrayList<>();
		zones.add(new Organisation("Z1a", "Zone", "z1a101"));
		
		Organisation comp = organizationRepository.findByCode(company.getCode());
		comp.setChild(zones);
		
		organizationRepository.save(comp);
		
		/*Add Asset*/
		List<Asset> assetList = new ArrayList<Asset>();
		assetList.add(new Asset("ast1"));
		Organisation zonedb = organizationRepository.findByCode("z1a101");
		zonedb.setAsset(assetList);
		organizationRepository.save(zonedb);
		
		
		Organisation zonefromdb = organizationRepository.findByCode("z1a101");
		List<Asset> assets = zonefromdb.getAsset();
		for(Asset a : assets) {
			if(a.getName()=="ast1") {
				assertEquals("ast1", a.getName());
				System.out.println("asset1 is connected");
			}
		}
		
	}
	
}
